package au.com.mivi.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.HashMap;

public class SessionManager {
    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;

    SharedPreferences.Editor editor;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "Mivi";

    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";

    // msn (make variable public to access from outside)
    public static final String KEY_MSN = "msn";

    // databalance (make variable public to access from outside)
    public static final String KEY_databalance = "databalance";

    // productname (make variable public to access from outside)
    public static final String KEY_productname = "name";

    // productprice (make variable public to access from outside)
    public static final String KEY_productprice = "price";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn) {

        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);

        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public void setuser(String msn, String databalance, String name, String price) {


        // Storing msn in pref
        editor.putString(KEY_MSN, msn);

        // Storing databalance in pref
        editor.putString(KEY_databalance, databalance);

        // Storing productname in pref
        editor.putString(KEY_productname, name);

        // Storing productprice in pref
        editor.putString(KEY_productprice, price);

        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        // msn
        user.put(KEY_MSN, pref.getString(KEY_MSN, null));

        // databalance
        user.put(KEY_databalance, pref.getString(KEY_databalance, null));

        // productname
        user.put(KEY_productname, pref.getString(KEY_productname, null));

        // productprice
        user.put(KEY_productprice, pref.getString(KEY_productprice, null));

        // return user
        return user;
    }
}

