package au.com.mivi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.HashMap;

import au.com.mivi.helper.SessionManager;

public class MainActivity extends AppCompatActivity {

    private TextView txtdatabalance;
    private TextView txtproductname;
    private TextView txtproductprice;

    private Button btnLogout;

    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtdatabalance = (TextView) findViewById(R.id.databalance);
        txtproductname = (TextView) findViewById(R.id.productname);
        txtproductprice = (TextView) findViewById(R.id.productprice);

        btnLogout = (Button) findViewById(R.id.btnLogout);

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // msn
        String msn = user.get(SessionManager.KEY_MSN);

        // databalance
        String databalance = user.get(SessionManager.KEY_databalance);

        // productname
        String productname = user.get(SessionManager.KEY_productname);

        // productprice
        String productprice = user.get(SessionManager.KEY_productprice);

        // Displaying the user details on the screen
        txtdatabalance.setText("Data Balance: " + databalance );
        txtproductname.setText("Product Name: " + productname );
        txtproductprice.setText("Product Price: " +productprice );


// Logout button click event
        btnLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data
     * */
    private void logoutUser() {
        session.setLogin(false);

        // Launching the login activity
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
