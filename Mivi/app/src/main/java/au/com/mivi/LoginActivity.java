package au.com.mivi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import au.com.mivi.helper.SessionManager;

import static android.content.ContentValues.TAG;
import static au.com.mivi.helper.Parser.getAssetJsonData;

public class LoginActivity extends Activity {

    private Button btnLogin;
    private EditText inputUserid;
    private ProgressDialog pDialog;
    private SessionManager session;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        inputUserid = (EditText) findViewById(R.id.userid);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Session manager
        session = new SessionManager(getApplicationContext());

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                String userid = inputUserid.getText().toString().trim();

                // Check for empty data in the form
                if (!userid.isEmpty()) {
                    // login user
                    checkLogin(userid);


                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please enter MSN", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });

    }

    /**
     * function to verify login details through json
     * */
    private void checkLogin(final String userid) {

        pDialog.setMessage("Logging in ...");
        showDialog();

        String data = getAssetJsonData(getApplicationContext());

        if (data != null) {
            try {
                JSONObject jsonObj = new JSONObject(data);

                // Getting JSON Array node "included"
                JSONArray included = jsonObj.getJSONArray("included");

                // Getting 1st array
                JSONObject result = included.getJSONObject(0);
                // Getting JSON Object "attributes"
                JSONObject attributes    = result.getJSONObject("attributes");
                // Getting key "msn"
                String msn = attributes.getString("msn");


                // Getting 2nd array "subscriptions"
                JSONObject subscriptions = included.getJSONObject(1);
                // Getting JSON Object "attributes"
                JSONObject subscriptions_attributes    = subscriptions.getJSONObject("attributes");
                // Getting key "included-data-balance"
                String databalance = subscriptions_attributes.getString("included-data-balance");

                // Getting 3rd array "products"
                JSONObject products = included.getJSONObject(2);
                // Getting JSON Object "attributes"
                JSONObject products_attributes    = products.getJSONObject("attributes");
                // Getting key "product name and price"
                String productname = products_attributes.getString("name");
                String productprice = products_attributes.getString("price");


                if(userid.equalsIgnoreCase(msn))
                {
                    // user successfully logged in
                    // Create login session
                    session.setLogin(true);


                    // set userdata
                    session.setuser(msn,databalance,productname,productprice);

                    // Take to main activity
                    Intent intent = new Intent(LoginActivity.this, Welcome.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Please Enter correct MSN" , Toast.LENGTH_LONG).show();
                }

                hideDialog();

            } catch (final JSONException e) {
                Log.e(TAG, "Json parsing error: " + e.getMessage());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Json parsing error: " + e.getMessage(),
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }
        }


    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
